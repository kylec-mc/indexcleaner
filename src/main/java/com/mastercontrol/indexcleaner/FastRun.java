package com.mastercontrol.indexcleaner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FastRun {

    public static void main(String[] args) throws Exception {
        final String PREFIX_TO_KILL = "promotion";



        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(
                "http://mcusdevsearch:9200/_cat/indices");
        HttpResponse response = httpClient.execute(getRequest);

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        int count = 0;
        String output;
        while ((output = br.readLine()) != null) {
            String index = output.split("[\\s]+")[2];
            if(index.startsWith(PREFIX_TO_KILL)) {
                IndexDeleter.delete(index);
            }
            count++;
        }

        System.out.println(count);

        httpClient.getConnectionManager().shutdown();
    }

    private static String allContent(BufferedReader br) throws IOException {
        String output = "";
        String output2;
        System.out.println("Output from Server .... \n");
        while ((output2 = br.readLine()) != null) {
            output += output2;
        }

        return output;
    }
}
