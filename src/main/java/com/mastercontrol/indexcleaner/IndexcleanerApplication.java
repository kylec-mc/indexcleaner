package com.mastercontrol.indexcleaner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexcleanerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(IndexcleanerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		CLIRunner.main(args);
	}
}
