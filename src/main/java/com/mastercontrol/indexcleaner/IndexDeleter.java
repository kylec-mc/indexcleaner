package com.mastercontrol.indexcleaner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class IndexDeleter {

    public static void delete(String index) {
        try {
            System.out.println("Deleting: " + index);
            HttpDelete deleteRequest = new HttpDelete("http://mcusdevsearch:9200/"+index);
            DefaultHttpClient httpClient2 = new DefaultHttpClient();
            HttpResponse response2 = httpClient2.execute(deleteRequest);

            if(response2.getStatusLine().getStatusCode() != 200) {
                String messedUp =  allContent(new BufferedReader(
                        new InputStreamReader((response2.getEntity().getContent()))));
                System.err.println(messedUp);
            }
            httpClient2.getConnectionManager().shutdown();
        } catch (Exception ex) {
         ex.printStackTrace();
        }

    }

    private static String allContent(BufferedReader br) throws IOException {
        String output = "";
        String output2;
        while ((output2 = br.readLine()) != null) {
            output += output2;
        }

        return output;
    }
}
