package com.mastercontrol.indexcleaner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BodyCleaner {

    public static void run() throws Exception {
        System.out.println("Cleaning dead indexes.");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(
                "http://mcusdevsearch:9200/_cat/indices");
        HttpResponse response = httpClient.execute(getRequest);

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        String output;
        while ((output = br.readLine()) != null) {
            String[] lineParts = output.split("[\\s]+");
            String index = lineParts[2];
            if(lineParts[0].equals("red")) {
                IndexDeleter.delete(index);
            }
        }

        httpClient.getConnectionManager().shutdown();
        System.out.println("Done cleaning dead indexes.");
    }

    public static void main(String[] args) throws Exception {
        run();
    }
}
