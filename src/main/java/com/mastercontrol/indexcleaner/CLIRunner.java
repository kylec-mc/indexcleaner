package com.mastercontrol.indexcleaner;

public class CLIRunner {
    public static void main(String[] args) throws Exception {
        if(args.length == 0) {
            printHelp();
        } else if("--bodies".equals(args[0])) {
            BodyCleaner.run();
        } else if("--promotion".equals(args[0])) {
            Ager.deleteOldPromotion();
        } else if("--old".equals(args[0])) {
            if(args.length < 3) {
                System.err.println("Deleting old items takes 3 parameters");
                System.exit(1);
            }
            if(Integer.parseInt(args[1]) < 1) {
                System.err.println("Versions to keep must be above 0.");
                System.exit(1);
            }
            if(args[2].length() == 0) {
                System.err.println("Index prefix must be at least 1 character long.");
                System.exit(1);
            }
            Ager.deleteOldVersions(Integer.parseInt(args[1]), args[2]);
        } else {
            printHelp();
        }
    }

    private static void printHelp() {
        System.out.println("Index Cleaner Help:\n"
                                   + "java -jar indexcleaner.jar --bodies\t<- Cleans up indexes that are broken\n"
                                   + "java -jar indexcleaner.jar --old <integer number of recents to keep> <prefix of "
                                   + "items to match>\t<- cleans up old indexes that match a prefix.\n"
                                   + "java -jar indexcleaner.jar --promotion\t<- Cleans up promotion sites");
    }
}
