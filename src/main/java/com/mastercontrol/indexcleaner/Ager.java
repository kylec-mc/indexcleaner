package com.mastercontrol.indexcleaner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Ager {

    public static void deleteOldPromotion() throws Exception {
        System.out.println("Cleaning old promotion indexes");
        for(int i=0; i<=20; i++) {
            cleanAllButLatestNVersions("promotion" + i + "_", 1);
        }
        System.out.println("Done cleaning old promotion indexes");
    }

    public static void deleteOldVersions(int versions, String prefix) throws Exception {
        System.out.println(String.format("Clearing out all but the last %s versions of indices prefixed with '%s'", versions, prefix));
        cleanAllButLatestNVersions(prefix, versions);
        System.out.println("Done clearing out old versions.");
    }

    public static void main(String[] args) throws Exception {
        deleteOldPromotion();
    }

    @Data
    @AllArgsConstructor
    private static class IndexData {
        private long creationData;
        private String indexName;
    }

    private static void cleanAllButLatestNVersions(String prefix, int versions) throws Exception {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(
                "http://mcusdevsearch:9200/_cat/indices");
        HttpResponse response = httpClient.execute(getRequest);

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        List<IndexData> data = new ArrayList<>();
        String output;
        while ((output = br.readLine()) != null) {
            String index = output.split("[\\s]+")[2];
            if(index.startsWith(prefix)) {
                DefaultHttpClient httpClient2 = new DefaultHttpClient();
                HttpGet getRequest2 = new HttpGet(
                        "http://mcusdevsearch:9200/"+index);
                HttpResponse response2 = httpClient2.execute(getRequest2);

                String stringContent = allContent(new BufferedReader(
                        new InputStreamReader((response2.getEntity().getContent()))));

                ObjectMapper mapper = new ObjectMapper();

                JsonNode node = mapper.readTree(stringContent);
                JsonNode nodeContent = node.elements().next();

                String indexName = node.fields().next().getKey();
                long creationData = nodeContent.get("settings").get("index").get("creation_date").asLong();

                IndexData thisData = new IndexData(creationData, indexName);
                data.add(thisData);

                httpClient2.getConnectionManager().shutdown();
            }
        }

        data.sort((o1, o2) -> {
            if(o1.creationData > o2.creationData) {
                return 1;
            } else {
                return -1;
            }
        });

        if(!data.isEmpty() && versions < data.size()) {
            data.stream().limit(data.size() - (long)versions).forEach(value -> IndexDeleter.delete(value.indexName));
        }


        httpClient.getConnectionManager().shutdown();
    }

    private static String allContent(BufferedReader br) throws IOException {
        String output = "";
        String output2;
        while ((output2 = br.readLine()) != null) {
            output += output2;
        }

        return output;
    }
}
