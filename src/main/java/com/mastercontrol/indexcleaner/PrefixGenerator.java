package com.mastercontrol.indexcleaner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PrefixGenerator {
    public static void main(String[] args) throws Exception {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(
                "http://mcusdevsearch:9200/_cat/indices");
        HttpResponse response = httpClient.execute(getRequest);

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        Map<String, Integer> prefixes = new HashMap<>();
        int count = 0;
        String output;
        while ((output = br.readLine()) != null) {
            String index = output.split("[\\s]+")[2];
            String prefix = index.substring(0, index.lastIndexOf('_'));
            prefixes.putIfAbsent(prefix, 0);
            Integer currentCount = prefixes.get(prefix);
            prefixes.put(prefix, currentCount + 1);

            count++;
        }

        System.out.println(count);
        prefixes.forEach((key, value) -> {
            System.out.println(key + ": " + value);
        });

        httpClient.getConnectionManager().shutdown();
    }
}
